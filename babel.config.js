module.exports = api => {
    return api.env("test") ? {
        presets: [["@babel/preset-env", {targets: {node: "current"}}], "@babel/preset-typescript"]
    }: {};
}